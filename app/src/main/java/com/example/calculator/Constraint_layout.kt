package com.example.calculator

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.databinding.ActivityConstraintLayoutBinding


class Constraint_layout : AppCompatActivity() {
    private lateinit var binding: ActivityConstraintLayoutBinding
    private var listener = OnClickListener()
    private lateinit var textView : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConstraintLayoutBinding.inflate(layoutInflater)
        textView = binding.textView

        savedInstanceState?.apply {
            textView.text = getString("value")
        }

        val buttons : Array<Button> = arrayOf(binding.button1, binding.button2,
            binding.button3, binding.button4, binding.button5, binding.button6,
            binding.button7, binding.button8, binding.button9, binding.button10,
            binding.button11, binding.button12)

        for (button in buttons) {
            button.setOnClickListener { listener.onButtonClick(button, textView) }
        }

        setContentView(binding.root)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("value", textView.text.toString())
    }
}
