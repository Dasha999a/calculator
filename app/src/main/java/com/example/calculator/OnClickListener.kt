package com.example.calculator

import android.widget.Button
import android.widget.TextView

class OnClickListener {
    fun onButtonClick(button : Button, textView : TextView) {
        textView.append(button.text.toString())
    }
}