package com.example.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.linearLayout.setOnClickListener {
            Intent(this, LinearLayout::class.java).also {
                startActivity(it)
            }
        }

        binding.frameLayout.setOnClickListener {
            Intent(this, FrameLayout::class.java).also {
                startActivity(it)
            }
        }

        binding.constraintLayout.setOnClickListener{
            Intent(this, Constraint_layout::class.java).also {
                startActivity(it)
            }
        }

    }
}